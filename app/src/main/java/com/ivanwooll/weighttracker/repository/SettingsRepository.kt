package com.ivanwooll.weighttracker.repository

import androidx.lifecycle.LiveData
import com.ivanwooll.weighttracker.App
import com.ivanwooll.weighttracker.WeightUnit
import com.ivanwooll.weighttracker.database.Database
import com.ivanwooll.weighttracker.model.WeightUnitModel
import javax.inject.Inject

/**
 * Created by ivanwooll on 17/02/2018.
 */
class SettingsRepository {

    @Inject
    lateinit var database: Database

    init {
        App.appComponent.inject(this)
    }

    private val weightUnitDao by lazy { database.weightUnitDao }

    companion object {
        const val ID = 1
    }

    fun initDbIfEmpty() {
            weightUnitDao.insert(WeightUnitModel(ID, WeightUnit.Stones.name))
    }

    fun set(weightUnit: WeightUnit) {
        val unit = when (weightUnit) {

            WeightUnit.Stones -> WeightUnitModel(ID, WeightUnit.Stones.name)
            WeightUnit.Lbs -> WeightUnitModel(ID, WeightUnit.Lbs.name)
            WeightUnit.Kilos -> WeightUnitModel(ID, WeightUnit.Kilos.name)
        }
        weightUnitDao.setWeightUnit(unit)
    }

    fun get(): LiveData<WeightUnitModel> {
        return weightUnitDao.get()
    }
}