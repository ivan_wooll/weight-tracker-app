package com.ivanwooll.weighttracker.repository

import androidx.lifecycle.LiveData
import com.ivanwooll.weighttracker.App
import com.ivanwooll.weighttracker.database.Database
import com.ivanwooll.weighttracker.model.Entry
import javax.inject.Inject

/**
 * Created by ivanwooll on 28/01/2018.
 */
class EntryRepository {

    @Inject
    lateinit var database: Database


    init {
        App.appComponent.inject(this)
    }

    private val entryDao by lazy { database.entryDao }

    fun getLastEntry(): Float {
        val allList = entryDao.getAllList()
        return if (allList.isNotEmpty()) {
            allList.last().weight
        } else {
            0F
        }
    }

    fun addEntryToDb(entry: Entry) {
        entryDao.insert(entry)
    }

    fun getAll(): LiveData<List<Entry>> {
        return entryDao.getAllLiveData()
    }
}