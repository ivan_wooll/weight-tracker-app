package com.ivanwooll.weighttracker.livedata;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.ivanwooll.weighttracker.enter_weight.EnterValuesViewModel;
import com.ivanwooll.weighttracker.entries.EntriesViewModel;
import com.ivanwooll.weighttracker.settings.SettingsViewModel;

/**
 * Created by ivanwooll on 11/02/2018.
 */

public class ViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(EntriesViewModel.class)) {
            return (T) new EntriesViewModel();
        } else if (modelClass.isAssignableFrom(SettingsViewModel.class)) {
            return (T) new SettingsViewModel();
        } else if (modelClass.isAssignableFrom(EnterValuesViewModel.class)) {
            return (T) new EnterValuesViewModel();
        }

        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
