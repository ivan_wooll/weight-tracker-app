package com.ivanwooll.weighttracker.enter_weight

import com.ivanwooll.weighttracker.base.BaseViewModel
import com.ivanwooll.weighttracker.repository.SettingsRepository

class EnterWeightViewModel : BaseViewModel() {


    private val settingsRepository by lazy { SettingsRepository() }


    val settings = settingsRepository.get()

}