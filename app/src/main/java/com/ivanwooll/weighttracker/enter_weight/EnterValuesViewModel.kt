package com.ivanwooll.weighttracker.enter_weight

import androidx.lifecycle.MutableLiveData
import com.ivanwooll.weighttracker.base.BaseViewModel
import com.ivanwooll.weighttracker.model.Entry
import com.ivanwooll.weighttracker.repository.EntryRepository
import kotlinx.coroutines.launch
import kotlin.properties.Delegates

class EnterValuesViewModel : BaseViewModel() {

    private val entryRepository by lazy { EntryRepository() }

    val lastEntryLiveData = MutableLiveData<Float>()
    val inputValidLiveData = MutableLiveData<Boolean>()

    var weight: Float by Delegates.observable(0F) { _, _, newValue ->
        inputValidLiveData.postValue(newValue != 0F)
    }

    fun fetchLastEntry() {
        launch {
            val lastEntry = entryRepository.getLastEntry()
            lastEntryLiveData.postValue(lastEntry)
        }
    }

    fun addEntryToDb(entry: Entry) {
        launch {
            entryRepository.addEntryToDb(entry)
        }
    }
}


