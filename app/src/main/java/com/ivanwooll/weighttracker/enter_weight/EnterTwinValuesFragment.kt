package com.ivanwooll.weighttracker.enter_weight

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.ivanwooll.weighttracker.R
import com.ivanwooll.weighttracker.afterTextChanged
import com.ivanwooll.weighttracker.livedata.ViewModelFactory
import com.ivanwooll.weighttracker.model.Entry
import com.ivanwooll.weighttracker.observeLiveData
import kotlinx.android.synthetic.main.fragment_enter_twin_values.*

/**
 * Created by ivanwooll on 28/01/2018.
 */
class EnterTwinValuesFragment : Fragment() {

    private var stones = 0
    private var lbs = 0F
    private var weight = 0F

    private val viewModel by lazy {
        ViewModelProviders
                .of(this, ViewModelFactory())
                .get(EnterValuesViewModel::class.java)
    }

    private lateinit var callback: EnterWeight.Callback

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_enter_twin_values, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchLastEntry()

        observeLiveData(viewModel.lastEntryLiveData) { lastEntry ->
            val stn = lastEntry / 14
            val lb = lastEntry.rem(14)

            editTextStones.setText(stn.toInt().toString())
            editTextLbs.setText("%.1f".format(lb))
            editTextLbs.setSelection(editTextLbs.length())

        }

        observeLiveData(viewModel.inputValidLiveData) {
            button.isEnabled = it
        }

        editTextStones.afterTextChanged {
            stones = if (it.isNotEmpty())
                (it.toInt() * 14)
            else
                0
        }

        editTextLbs.afterTextChanged {
            lbs = if (it.isNotEmpty())
                it.toFloat()
            else
                0F
        }

        editTextLbs.requestFocus()

        button.setOnClickListener {
            if (!validateFields()) {
                return@setOnClickListener
            }
            addEntryToDb()
            callback.onWeightEntered()
        }

    }

    private fun addEntryToDb() {
        weight = stones + lbs
        val entry = Entry(System.currentTimeMillis(), weight)
        viewModel.addEntryToDb(entry)
    }

    private fun validateFields(): Boolean {
        return stones != 0
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callback = context as EnterWeight.Callback
        } catch (exception: ClassCastException) {
            throw ClassCastException("$context must implement EnterWeight.Callback")
        }
    }

}