package com.ivanwooll.weighttracker.enter_weight

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ivanwooll.weighttracker.*
import com.ivanwooll.weighttracker.model.Entry
import kotlinx.android.synthetic.main.fragment_enter_single_value.*

/**
 * Created by ivanwooll on 02/02/2018.
 */
class EnterSingleValueFragment : Fragment() {

    private val viewModel by lazy {
        createViewModel<EnterValuesViewModel>()
    }


    private lateinit var callback: EnterWeight.Callback

    private val valueType: ValueType? by lazy {
        when (arguments?.getInt(VALUE_TYPE, -1)) {
            0 -> ValueType.Lbs
            1 -> ValueType.Kgs
            else -> {
                null
            }
        }
    }

    companion object {
        private const val VALUE_TYPE = "value_type"
        fun newInstance(valueType: ValueType): EnterSingleValueFragment {
            return EnterSingleValueFragment().apply {
                val bundle = Bundle()
                val i = when (valueType) {
                    ValueType.Lbs -> 0
                    ValueType.Kgs -> 1
                }
                bundle.putInt(VALUE_TYPE, i)
                arguments = bundle
            }
        }
    }

    sealed class ValueType {
        object Lbs : ValueType()
        object Kgs : ValueType()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_enter_single_value, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchLastEntry()

        observeLiveData(viewModel.lastEntryLiveData) { lastEntry ->
            when (valueType) {
                ValueType.Kgs -> {
                    val d = lastEntry * 0.453592
                    editTextSingleVal.setText("%.1f".format(d))
                    editTextSingleVal.setSelection(editTextSingleVal.length())
                }
                ValueType.Lbs -> {
                    editTextSingleVal.setText(lastEntry.toString())
                    editTextSingleVal.setSelection(editTextSingleVal.length())
                }
            }
        }

        observeLiveData(viewModel.inputValidLiveData) {
            button.isEnabled = it
        }

        when (valueType) {
            ValueType.Lbs -> {
                textViewSingleLabel.hint = getString(R.string.lbs)
            }
            ValueType.Kgs -> {
                textViewSingleLabel.hint = getString(R.string.kgs)
            }
            null -> throw IllegalArgumentException("valueType not recognised")
        }

        editTextSingleVal.afterTextChanged {
            viewModel.weight = if (it.isNotEmpty())
                it.toFloat()
            else
                0F
        }

        button.setOnClickListener {
            if (!validateFields()) {
                return@setOnClickListener
            }
            addEntryToDb()
            callback.onWeightEntered()
        }
    }

    private fun addEntryToDb() {
        val entry = Entry(System.currentTimeMillis(), viewModel.weight)
        viewModel.addEntryToDb(entry)
    }


    private fun validateFields(): Boolean {
        return viewModel.weight != 0f
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callback = context as EnterWeight.Callback
        } catch (exception: ClassCastException) {
            throw ClassCastException("$context must implement EnterWeight.Callback")
        }
    }
}