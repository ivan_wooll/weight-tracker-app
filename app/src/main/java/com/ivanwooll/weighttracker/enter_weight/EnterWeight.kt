package com.ivanwooll.weighttracker.enter_weight

/**
 * Created by ivanwooll on 28/01/2018.
 */
interface EnterWeight {

    interface Callback {
        fun onWeightEntered()
    }

    fun displayLastEntry()
    fun addEntryToDb()
    fun validateFields(): Boolean
}