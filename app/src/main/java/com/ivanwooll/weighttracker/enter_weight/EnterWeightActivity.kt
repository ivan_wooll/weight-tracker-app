package com.ivanwooll.weighttracker.enter_weight

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import com.ivanwooll.weighttracker.R
import com.ivanwooll.weighttracker.WeightUnit
import com.ivanwooll.weighttracker.createViewModel
import com.ivanwooll.weighttracker.observeLiveData
import kotlinx.android.synthetic.main.activity_settings.*

/**
 * Created by ivanwooll on 22/01/2018.
 */

// Because it's possible to enter weight values in multiple ways I load the appropriate fragment in to this Activity depending on the
// chosen weigh unit

class EnterWeightActivity : AppCompatActivity(), EnterWeight.Callback {

    private val viewModel by lazy { createViewModel<EnterWeightViewModel>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_enter_weight)

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        observeLiveData(viewModel.settings) {


            val fragment: Fragment? = when (it.weightUnit) {
                WeightUnit.Stones.name -> EnterTwinValuesFragment()
                WeightUnit.Lbs.name -> EnterSingleValueFragment.newInstance(EnterSingleValueFragment.ValueType.Lbs)
                WeightUnit.Kilos.name -> EnterSingleValueFragment.newInstance(EnterSingleValueFragment.ValueType.Kgs)
                else -> null
            }
            fragment?.let { fragment ->
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.add(R.id.fragmentContainer, fragment, null).commit()
            }
        }

    }

    override fun onWeightEntered() {
        finish()
    }
}

