package com.ivanwooll.weighttracker

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

/**
 * Created by ivanwooll on 23/01/2018.
 */
fun Any.debugLog() {

    if (!BuildConfig.DEBUG) return

    val fullClassName = Thread.currentThread().stackTrace[3].className
    val className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1)
    val methodName = Thread.currentThread().stackTrace[3].methodName
    val lineNumber = Thread.currentThread().stackTrace[3].lineNumber

    Log.d("*** DEBUG_LOG *** $className.$methodName():$lineNumber ", this.toString())
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun Instant.format(): String {
    val formatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
            .withLocale(Locale.UK)
            .withZone(ZoneId.systemDefault())
    return formatter.format(this)
}

fun Float.formatAsWeight(weightUnit: WeightUnit): String {
    return when (weightUnit) {
        WeightUnit.Stones -> {
            val stones = (this / 14).toInt()
            val lb = this.rem(14)
            "$stones st, ${"%.1f".format(lb)} lb"
        }
        WeightUnit.Lbs -> "$this lbs"

        WeightUnit.Kilos -> {
            val d = this * 0.453592
            "${"%.1f".format(d)} kg"
        }
    }

}

fun <T> LifecycleOwner.observeLiveData(liveData: LiveData<T>, function: (T) -> Unit) {
    liveData.observe(this, { t: T -> function.invoke(t) })
}

inline fun <reified T : ViewModel> AppCompatActivity.createViewModel(): T {
    return ViewModelProviders.of(this).get(T::class.java)
}

inline fun <reified T : ViewModel> Fragment.createViewModel(): T {
    return ViewModelProviders.of(this).get(T::class.java)
}

