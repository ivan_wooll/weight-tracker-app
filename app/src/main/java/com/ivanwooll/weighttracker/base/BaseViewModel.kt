package com.ivanwooll.weighttracker.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

// Base ViewModel class that provides a default CoroutineScope to run coroutines on the IO Dispatcher that runs operations on a background thread.
// Because of this you should always call LiveData.postValue inside launch{} scope to ensure that updates are made on the UI thread
open class BaseViewModel : ViewModel(), CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext get() = job + Dispatchers.IO


    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}