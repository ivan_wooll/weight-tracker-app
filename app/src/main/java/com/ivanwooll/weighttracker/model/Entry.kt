package com.ivanwooll.weighttracker.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by ivanwooll on 22/01/2018.
 */
@Entity(tableName = "entries")
data class Entry(@PrimaryKey val timeStamp: Long, val weight: Float)