package com.ivanwooll.weighttracker.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ivanwooll.weighttracker.WeightUnit

/**
 * Created by ivanwooll on 17/02/2018.
 */
@Entity(tableName = "weight_unit")
data class WeightUnitModel(@PrimaryKey var id: Int, var weightUnit: String) {
    constructor() : this(1, WeightUnit.Stones.name)
}