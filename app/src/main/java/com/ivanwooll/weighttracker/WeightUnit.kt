package com.ivanwooll.weighttracker

/**
 * Created by ivanwooll on 27/01/2018.
 */
enum class WeightUnit {
    Stones, Lbs, Kilos
}