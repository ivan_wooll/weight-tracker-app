package com.ivanwooll.weighttracker.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ivanwooll.weighttracker.model.WeightUnitModel

/**
 * Created by ivanwooll on 17/02/2018.
 */
@Dao
interface WeightUnitDao {

    // allow insertion only once to init the single entry
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(weightUnitModel: WeightUnitModel)

    @Update
    fun setWeightUnit(weightUnitModel: WeightUnitModel)

    @Query("SELECT * FROM weight_unit WHERE id = 1")
    fun get(): LiveData<WeightUnitModel>

}