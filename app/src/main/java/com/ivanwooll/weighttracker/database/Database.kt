package com.ivanwooll.weighttracker.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ivanwooll.weighttracker.model.Entry
import com.ivanwooll.weighttracker.model.WeightUnitModel

/**
 * Created by ivanwooll on 23/01/2018.
 */
@Database(entities = [Entry::class, WeightUnitModel::class], version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {

    abstract val entryDao: EntryDao

    abstract val weightUnitDao: WeightUnitDao
}