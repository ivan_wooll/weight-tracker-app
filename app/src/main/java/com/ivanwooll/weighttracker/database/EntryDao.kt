package com.ivanwooll.weighttracker.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ivanwooll.weighttracker.model.Entry

/**
 * Created by ivanwooll on 23/01/2018.
 */
@Dao
interface EntryDao {

    @Insert
    fun insert(entry: Entry)

    @Query("SELECT * FROM entries")
    fun getAllList(): List<Entry>


    @Query("SELECT * FROM entries")
    fun getAllLiveData(): LiveData<List<Entry>>
}