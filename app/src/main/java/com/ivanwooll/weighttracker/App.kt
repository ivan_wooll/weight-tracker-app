package com.ivanwooll.weighttracker

import android.app.Application
import com.ivanwooll.weighttracker.dagger.AppComponent
import com.ivanwooll.weighttracker.dagger.ContextModule
import com.ivanwooll.weighttracker.dagger.DaggerAppComponent

/**
 * Created by ivanwooll on 23/01/2018.
 */
class App : Application() {

    companion object {

        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
//        JodaTimeAndroid.init(this)

        appComponent = DaggerAppComponent
                .builder()
                .contextModule(ContextModule(this))
                .build()
    }
}