package com.ivanwooll.weighttracker.entries

import com.ivanwooll.weighttracker.base.BaseViewModel
import com.ivanwooll.weighttracker.repository.EntryRepository
import com.ivanwooll.weighttracker.repository.SettingsRepository
import kotlinx.coroutines.launch

/**
 * Created by ivanwooll on 07/02/2018.
 */

class EntriesViewModel : BaseViewModel() {

    private val entryRepository by lazy { EntryRepository() }
    private val settingsRepository by lazy { SettingsRepository() }

    val observableEntries = entryRepository.getAll()
    val observableWeightUnit = settingsRepository.get()

    fun initDbIfEmpty() {
        launch {
            settingsRepository.initDbIfEmpty()
        }
    }

}
