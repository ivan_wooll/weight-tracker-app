package com.ivanwooll.weighttracker.entries

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ivanwooll.weighttracker.R
import com.ivanwooll.weighttracker.WeightUnit
import com.ivanwooll.weighttracker.enter_weight.EnterWeightActivity
import com.ivanwooll.weighttracker.livedata.ViewModelFactory
import com.ivanwooll.weighttracker.settings.SettingsActivity
import kotlinx.android.synthetic.main.activity_main.*

class EntriesActivity : AppCompatActivity() {

    private val entryAdapter by lazy { EntryAdapter() }

    private val viewModel by lazy {
        ViewModelProviders
                .of(this, ViewModelFactory())
                .get(EntriesViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.initDbIfEmpty()
        setSupportActionBar(toolbar)


        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = entryAdapter

        floatingActionButton.setOnClickListener {
            startActivity(Intent(this, EnterWeightActivity::class.java))
        }



        viewModel.observableEntries.observe(this, Observer { list ->
            list?.let {
                group.visibility = if (list.isEmpty()) View.VISIBLE else View.GONE
                entryAdapter.entries.clear()
                entryAdapter.entries.addAll(list)
                entryAdapter.notifyDataSetChanged()
            }
        })

        viewModel.observableWeightUnit.observe(this, Observer { weightUnitModel ->
            weightUnitModel?.let {
                val weightUnit: WeightUnit = when (it.weightUnit) {
                    WeightUnit.Lbs.name -> WeightUnit.Lbs
                    WeightUnit.Kilos.name -> WeightUnit.Kilos
                    else -> {
                        WeightUnit.Stones
                    }
                }
                entryAdapter.updateWeightUnit(weightUnit)
            }
        })


    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
            }

        }
        return super.onOptionsItemSelected(item)
    }
}
