package com.ivanwooll.weighttracker.entries

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ivanwooll.weighttracker.R
import com.ivanwooll.weighttracker.WeightUnit
import com.ivanwooll.weighttracker.formatAsWeight
import com.ivanwooll.weighttracker.format

import com.ivanwooll.weighttracker.model.Entry
import kotlinx.android.synthetic.main.list_item.view.*
import java.time.Instant
import java.util.*

/**
 * Created by ivanwooll on 22/01/2018.
 */

class EntryAdapter : RecyclerView.Adapter<EntryAdapter.ViewHolder>() {

    val entries = ArrayList<Entry>()
    private var weightUnit = WeightUnit.Stones

    fun updateWeightUnit(weightUnit: WeightUnit) {
        this.weightUnit = weightUnit
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (timeStamp, weight) = entries[position]

        val date = Instant.ofEpochMilli(timeStamp)



        holder.itemView.textViewTimeStamp.text = date.format()
        holder.itemView.textViewWeight.text = weight.formatAsWeight(weightUnit)

    }

    override fun getItemCount(): Int {
        return entries.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
