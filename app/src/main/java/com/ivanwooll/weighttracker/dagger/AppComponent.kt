package com.ivanwooll.weighttracker.dagger

import com.ivanwooll.weighttracker.repository.EntryRepository
import com.ivanwooll.weighttracker.repository.SettingsRepository
import dagger.Component
import javax.inject.Singleton

// I've tended to use Dagger only at the Repository or "Manager" level as a way of having a
// consistent way to build this type of class and provide it with dependencies

@Component(modules = [ContextModule::class, DatabaseModule::class])
@Singleton
interface AppComponent {
    fun inject(entryRepository: EntryRepository)
    fun inject(entryRepository: SettingsRepository)
}