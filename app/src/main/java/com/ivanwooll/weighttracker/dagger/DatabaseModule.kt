package com.ivanwooll.weighttracker.dagger

import androidx.room.Room
import android.content.Context
import com.ivanwooll.weighttracker.database.Database
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context):Database{
      return  Room.databaseBuilder(context, Database::class.java, "main-db").build()
    }

}