package com.ivanwooll.weighttracker.settings

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.ivanwooll.weighttracker.WeightUnit
import com.ivanwooll.weighttracker.base.BaseViewModel
import com.ivanwooll.weighttracker.model.WeightUnitModel
import com.ivanwooll.weighttracker.repository.SettingsRepository
import kotlinx.coroutines.launch

/**
 * Created by ivanwooll on 17/02/2018.
 */
class SettingsViewModel : BaseViewModel() {
    fun set(unit: WeightUnit) {
        launch { settingsRepository.set(unit) }
    }

    val observableWeightUnit = MediatorLiveData<WeightUnitModel>()

    private val settingsRepository by lazy { SettingsRepository() }

    init {
        observableWeightUnit.value = null

        val weightUnit = settingsRepository.get()
        observableWeightUnit.addSource(weightUnit, observableWeightUnit::setValue)
    }
}