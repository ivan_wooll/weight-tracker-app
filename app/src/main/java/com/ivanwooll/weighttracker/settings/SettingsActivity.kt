package com.ivanwooll.weighttracker.settings

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ivanwooll.weighttracker.R
import com.ivanwooll.weighttracker.WeightUnit
import com.ivanwooll.weighttracker.livedata.ViewModelFactory
import com.ivanwooll.weighttracker.repository.SettingsRepository
import kotlinx.android.synthetic.main.activity_settings.*

/**
 * Created by ivanwooll on 28/01/2018.
 */
class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_settings)

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val viewModel = ViewModelProviders
                .of(
                        this,
                        ViewModelFactory()
                ).get(SettingsViewModel::class.java)
        viewModel.observableWeightUnit.observe(this, Observer { t ->
            t?.let {
                val id = when (it.weightUnit) {
                    WeightUnit.Kilos.name -> R.id.radioButton2
                    WeightUnit.Lbs.name -> R.id.radioButton3
                    else -> R.id.radioButton1
                }
                radioGroup.check(id)
            }

        })

        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            val unit = when (checkedId) {
                R.id.radioButton2 -> WeightUnit.Kilos
                R.id.radioButton3 -> WeightUnit.Lbs
                else -> WeightUnit.Stones
            }
            viewModel.set(unit)
        }
    }
}